package com.itnove.order.bo;

import com.itnove.order.bo.exception.BOException;
import com.itnove.order.dao.OrderDAO;
import com.itnove.order.dto.Order;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by guillem on 22/02/16.
 */
public class OrderBOImplTest {

    @Mock OrderDAO dao;
    private OrderBOImpl bo;
    Order order;

    @Before public void setup() {
        MockitoAnnotations.initMocks(this);
        bo = new OrderBOImpl();
        bo.setDao(dao);
        order = new Order();
    }

    @Test public void placeOrder_Should_Create_An_Order() throws SQLException, BOException {
	// When Dao creates and order returns 1
        // Assert true in placeOrder
        // Veriy dao create order
    }

    @Test public void placeOrder_Should_Not_Create_An_Order() throws SQLException, BOException {
    }

    @Test (expected = BOException.class)
    public void placeOrder_Should_Throw_Exception() throws SQLException, BOException {
    }
}
